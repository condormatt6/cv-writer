import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import VueRouter from 'vue-router'
import Routes from './Routes'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faDesktop } from '@fortawesome/free-solid-svg-icons'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { faFile } from '@fortawesome/free-solid-svg-icons'



import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faDesktop)
library.add(faEdit)
library.add(faFile)


Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueRouter)

const router = new VueRouter({
  routes: Routes,
  mode:'history'
});

Vue.config.productionTip = false


new Vue({
  render: h => h(App),
  router: router,
}).$mount('#app')
