import Contenu from './components/Contenu.vue'
import About from './components/Pages/About.vue'
import Services from './components/Pages/Services.vue'
import Templates from './components/Pages/Templates.vue'

export default [
    {path:'/',component:Contenu},
    {path:'/Templates',component:Templates},
    { path: '/Services', component: Services },
    {path:'/About',component:About}
];